package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class SittingStanding : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("sittingAndStanding")
    var sittingAndStanding: String? = null

    var exam: String? = null
}
