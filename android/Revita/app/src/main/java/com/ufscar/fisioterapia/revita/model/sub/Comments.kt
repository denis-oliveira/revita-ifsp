package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey


open class Comments : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("dropped")
    var dropped: String? = null

    @SerializedName("numberFalls")
    var numberFalls: String? = null

    @SerializedName("medicines")
    var medicines: String? = null

    @SerializedName("currentPhysicalActivity")
    var currentPhysicalActivity: String? = null

    @SerializedName("otherComments")
    var otherComments: String? = null

    var exam: String? = null
}
