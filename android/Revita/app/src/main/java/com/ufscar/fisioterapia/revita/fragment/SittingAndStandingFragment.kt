package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Chronometer
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.sub.SittingStanding
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SittingAndStandingFragment : Fragment(), View.OnClickListener, Chronometer.OnChronometerTickListener {

    private var edtSittingAndStanding: TextInputEditText? = null
    private var chronometer: Chronometer? = null
    private var btnAdd: AppCompatButton? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater?.inflate(R.layout.fragment_sitting_and_standing, container, false)

        edtSittingAndStanding = view?.findViewById(R.id.edt_sitting_and_standing) as TextInputEditText?

        chronometer = view?.findViewById(R.id.chronometer) as Chronometer?
        chronometer?.setOnClickListener(this)
        chronometer?.onChronometerTickListener = this

        val fragment = InformationFragment()
        fragment.information = context.getString(R.string.inst_sitting_and_standing)

        val btnHelp: AppCompatImageButton? = view?.findViewById(R.id.btn_help) as AppCompatImageButton
        btnHelp?.setOnClickListener {
            activity.supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_content_main, fragment)
                    .commit()
        }

        btnAdd = view?.findViewById(R.id.btn_add) as AppCompatButton?
        btnAdd?.setOnClickListener(this)

        return view
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.chronometer -> {
                chronometer?.base = SystemClock.elapsedRealtime()
                chronometer?.start()
            }
            R.id.btn_add -> save()
        }
    }

    override fun onChronometerTick(chronometer: Chronometer?) {
        if (chronometer?.text.toString().equals("00:30", ignoreCase = true)) {
            // chronometer?.setOnClickListener(null)
            chronometer?.stop()
            Toast.makeText(context, "Terminou", Toast.LENGTH_SHORT).show()
        }
    }

    fun save() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val sittingStanding = SittingStanding()
        sittingStanding.sittingAndStanding = edtSittingAndStanding?.text.toString()

        val id = RevitaApplication.instance?.currentExam ?: ""

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.sittingStanding(id, sittingStanding)
        call.enqueue(object : Callback<SittingStanding> {
            override fun onResponse(call: Call<SittingStanding>, response: Response<SittingStanding>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<SittingStanding>, t: Throwable) {
                // Save to sync
                sittingStanding.exam = id
                val realm = DatabaseFactory.createInstance(context)
                realm.beginTransaction()
                realm.insertOrUpdate(sittingStanding)
                realm.commitTransaction()

                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Sitting and Standing Fragment"))
    }

    companion object {
        private val TAG = "SITTING_AND_STANDING_FRAGMENT"
    }
}
