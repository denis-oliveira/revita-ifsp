package com.ufscar.fisioterapia.revita.service

import com.ufscar.fisioterapia.revita.model.sub.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PhysicalExamService {

    @POST("/api/physical_assessment/base")
    fun create(@Body baseInformation: BaseInformation): Call<BaseInformation>

    @GET("/api/physical_assessment/{id}/base")
    fun baseInformation(@Path("id") id: String): Call<BaseInformation>

    @GET("/api/physical_assessment/{id}/blood-pressure-heart-rate")
    fun bloodPressureHeartRate(@Path("id") id: String): Call<BloodPressureHeartRate>

    @POST("/api/physical_assessment/{id}/blood-pressure-heart-rate")
    fun bloodPressureHeartRate(@Path("id") id: String, @Body bloodPressureHeartRate: BloodPressureHeartRate): Call<BloodPressureHeartRate>

    @GET("/api/physical_assessment/{id}/body-mass-height")
    fun bodyMassHeight(@Path("id") id: String): Call<BodyMassHeight>

    @POST("/api/physical_assessment/{id}/body-mass-height")
    fun bodyMassHeight(@Path("id") id: String, @Body bodyMassHeight: BodyMassHeight): Call<BodyMassHeight>

    @GET("/api/physical_assessment/{id}/bioimpedance")
    fun bioimpedance(@Path("id") id: String): Call<Bioimpedance>

    @POST("/api/physical_assessment/{id}/bioimpedance")
    fun bioimpedance(@Path("id") id: String, @Body bioimpedance: Bioimpedance): Call<Bioimpedance>

    @GET("/api/physical_assessment/{id}/sitting-standing")
    fun sittingStanding(@Path("id") id: String): Call<SittingStanding>

    @POST("/api/physical_assessment/{id}/sitting-standing")
    fun sittingStanding(@Path("id") id: String, @Body sittingStanding: SittingStanding): Call<SittingStanding>

    @GET("/api/physical_assessment/{id}/dynamometer-manual")
    fun dynamometerManual(@Path("id") id: String): Call<DynamometerManual>

    @POST("/api/physical_assessment/{id}/dynamometer-manual")
    fun dynamometerManual(@Path("id") id: String, @Body dynamometerManual: DynamometerManual): Call<DynamometerManual>

    @GET("/api/physical_assessment/{id}/trunk-flexibility")
    fun trunkFlexibility(@Path("id") id: String): Call<TrunkFlexibility>

    @POST("/api/physical_assessment/{id}/trunk-flexibility")
    fun trunkFlexibility(@Path("id") id: String, @Body trunkFlexibility: TrunkFlexibility): Call<TrunkFlexibility>

    @GET("/api/physical_assessment/{id}/balance")
    fun balance(@Path("id") id: String): Call<Balance>

    @POST("/api/physical_assessment/{id}/balance")
    fun balance(@Path("id") id: String, @Body balance: Balance): Call<Balance>

    @GET("/api/physical_assessment/{id}/walking")
    fun walking(@Path("id") id: String): Call<Walking>

    @POST("/api/physical_assessment/{id}/walking")
    fun walking(@Path("id") id: String, @Body walking: Walking): Call<Walking>

    @GET("/api/physical_assessment/{id}/comments")
    fun comments(@Path("id") id: String): Call<Comments>

    @POST("/api/physical_assessment/{id}/comments")
    fun comments(@Path("id") id: String, @Body comments: Comments): Call<Comments>
}
