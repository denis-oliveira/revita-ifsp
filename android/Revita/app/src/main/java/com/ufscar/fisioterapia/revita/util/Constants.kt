package com.ufscar.fisioterapia.revita.util

object Constants {
    val BASE_URL = "http://app.denisoliveira.com:8080/"
    // val BASE_URL = "http://192.168.0.18:8080/"
    val TIMEOUT = 60L
}
