package com.ufscar.fisioterapia.revita.service

import com.ufscar.fisioterapia.revita.model.User

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.POST
import retrofit2.http.PUT

interface UserService {

    @POST("user/auth")
    fun auth(@Body user: User): Call<User>

    @POST("user/reset")
    fun reset(@Body user: User): Call<User>

    @POST("user/")
    fun create(@Body user: User): Call<User>

    @PUT("user/")
    fun update(@Body user: User): Call<User>

    @DELETE("user/")
    fun delete(@Body user: User): Call<Boolean>
}
