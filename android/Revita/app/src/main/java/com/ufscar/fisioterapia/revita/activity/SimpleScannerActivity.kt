package com.ufscar.fisioterapia.revita.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log

import com.google.zxing.Result

import me.dm7.barcodescanner.zxing.ZXingScannerView

class SimpleScannerActivity : Activity(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        // Programmatically initialize the scanner view
        mScannerView = ZXingScannerView(this)
        // Set the scanner view as the content view
        setContentView(mScannerView)
    }

    public override fun onResume() {
        super.onResume()
        // Register ourselves as a handler for scan results.
        mScannerView?.setResultHandler(this)
        // Start camera on resume
        mScannerView?.startCamera()
    }

    public override fun onPause() {
        super.onPause()
        // Stop camera on pause
        mScannerView?.stopCamera()
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        Log.v(TAG, rawResult.text) // Prints scan results
        Log.v(TAG, rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode, pdf417 etc.)

        // If you would like to resume scanning, call this method below:
        // mScannerView?.resumeCameraPreview(this)
        intent.putExtra("RAW", rawResult.text)
        setResult(ON_SCAN, intent)
        finishActivity()
    }

    private fun finishActivity() {
        // To change body of created functions use File | Settings | File Templates.
        throw UnsupportedOperationException("not implemented")
    }

    interface SimpleScannerResult {
        fun result(raw: String)
    }

    companion object {
        private val TAG = "REVITA:SIMPLESCANNER"
        val ON_SCAN = 1
    }
}
