package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class TrunkFlexibility : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("trunkFlexibility")
    var trunkFlexibility: String? = null

    @SerializedName("abdominalCircumference")
    var abdominalCircumference: String? = null

    @SerializedName("hip")
    var hip: String? = null

    @SerializedName("waist")
    var waist: String? = null

    var exam: String? = null
}
