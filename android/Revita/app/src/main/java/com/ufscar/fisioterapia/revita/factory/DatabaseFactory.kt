package com.ufscar.fisioterapia.revita.factory

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration


class DatabaseFactory {
    companion object Factory {

        var db:Realm? = null

        fun createInstance(context: Context): Realm {
            if (db == null) {
                Realm.init(context)
                val config = RealmConfiguration.Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build()
                Realm.setDefaultConfiguration(config)
                db = Realm.getDefaultInstance()
            }
            return db as Realm
        }
    }
}
