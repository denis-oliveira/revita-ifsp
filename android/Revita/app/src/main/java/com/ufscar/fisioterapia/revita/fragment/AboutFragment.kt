package com.ufscar.fisioterapia.revita.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R


class AboutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        return inflater!!.inflate(R.layout.fragment_about, container, false)
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("About Fragment"))
    }

    companion object {
        private val TAG = "ABOUT_FRAGMENT"
    }
}
