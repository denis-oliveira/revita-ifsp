package com.ufscar.fisioterapia.revita.app

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.os.SystemClock
import com.ufscar.fisioterapia.revita.content.RevitaReceiver
import android.net.NetworkInfo
import android.net.ConnectivityManager
import android.widget.Toast
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.User
import com.ufscar.fisioterapia.revita.model.sub.*
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmQuery
import io.realm.RealmResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SyncService : Service() {

    // Binder given to clients
    private val binder = SyncBinder()
    private var alarmManager: AlarmManager? = null
    private var pendingAlarm: PendingIntent? = null

    var isRunning: Boolean = false

    override fun onCreate() {
        super.onCreate()
        service = this
        alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        pendingAlarm = PendingIntent.getBroadcast(this, 0, Intent(this, RevitaReceiver::class.java),
                0)
        alarmManager?.setRepeating(
                AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(),
                DELAY_TIME,
                pendingAlarm
        )
        isRunning = true
    }

    override fun onBind(intent: Intent?): IBinder {
        return binder
    }

    fun syncData() {
        if (RevitaApplication.instance?.applicationContext != null) {
            val realm = DatabaseFactory.createInstance(RevitaApplication.instance!!.applicationContext)
            isNetworkAvailable() && balanceSync(realm)
            isNetworkAvailable() && bioimpedanceSync(realm)
            isNetworkAvailable() && bloodPressureHeartRateSync(realm)
            isNetworkAvailable() && bodyMassHeightSync(realm)
            isNetworkAvailable() && commentsSync(realm)
            isNetworkAvailable() && dynamometerManualSync(realm)
            isNetworkAvailable() && sittingStandingSync(realm)
            isNetworkAvailable() && trunkFlexibilitySync(realm)
            isNetworkAvailable() && walkingSync(realm)
        }
    }

    private fun balanceSync(realm: Realm): Boolean {
        val query = realm.where(Balance::class.java)
        val results = query.findAll()
        for (balance in results.toList()) {
            balance.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.balance(balance.exam ?: "", balance)
            call.enqueue(object : Callback<Balance> {
                override fun onResponse(call: Call<Balance>, response: Response<Balance>) {
                    balance.deleteFromRealm()
                }
                override fun onFailure(call: Call<Balance>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun bioimpedanceSync(realm: Realm): Boolean {
        val query = realm.where(Bioimpedance::class.java)
        val results = query.findAll()
        for (bioimpedance in results.toList()) {
            bioimpedance.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.bioimpedance(bioimpedance.exam ?: "", bioimpedance)
            call.enqueue(object : Callback<Bioimpedance> {
                override fun onResponse(call: Call<Bioimpedance>, response: Response<Bioimpedance>) {
                   bioimpedance.deleteFromRealm()
                }
                override fun onFailure(call: Call<Bioimpedance>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun bloodPressureHeartRateSync(realm: Realm): Boolean {
        val query = realm.where(BloodPressureHeartRate::class.java)
        val results = query.findAll()
        for (bloodPressureHeartRate in results.toList()) {
            bloodPressureHeartRate.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.bloodPressureHeartRate(bloodPressureHeartRate.exam ?: "", bloodPressureHeartRate)
            call.enqueue(object : Callback<BloodPressureHeartRate> {
                override fun onResponse(call: Call<BloodPressureHeartRate>, response: Response<BloodPressureHeartRate>) {
                    bloodPressureHeartRate.deleteFromRealm()
                }
                override fun onFailure(call: Call<BloodPressureHeartRate>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun bodyMassHeightSync(realm: Realm): Boolean {
        val query = realm.where(BodyMassHeight::class.java)
        val results = query.findAll()
        for (bodyMassHeight in results.toList()) {
            bodyMassHeight.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.bodyMassHeight(bodyMassHeight.exam ?: "", bodyMassHeight)
            call.enqueue(object : Callback<BodyMassHeight> {
                override fun onResponse(call: Call<BodyMassHeight>, response: Response<BodyMassHeight>) {
                    bodyMassHeight.deleteFromRealm()
                }
                override fun onFailure(call: Call<BodyMassHeight>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun commentsSync(realm: Realm): Boolean {
        val query = realm.where(Comments::class.java)
        val results = query.findAll()
        for (comments in results.toList()) {
            comments.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.comments(comments.exam ?: "", comments)
            call.enqueue(object : Callback<Comments> {
                override fun onResponse(call: Call<Comments>, response: Response<Comments>) {
                    comments.deleteFromRealm()
                }
                override fun onFailure(call: Call<Comments>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun dynamometerManualSync(realm: Realm): Boolean {
        val query = realm.where(DynamometerManual::class.java)
        val results = query.findAll()
        for (dynamometerManual in results.toList()) {
            dynamometerManual.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.dynamometerManual(dynamometerManual.exam ?: "", dynamometerManual)
            call.enqueue(object : Callback<DynamometerManual> {
                override fun onResponse(call: Call<DynamometerManual>, response: Response<DynamometerManual>) {
                    dynamometerManual.deleteFromRealm()
                }
                override fun onFailure(call: Call<DynamometerManual>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun sittingStandingSync(realm: Realm): Boolean {
        val query = realm.where(SittingStanding::class.java)
        val results = query.findAll()
        for (sittingStanding in results.toList()) {
            sittingStanding.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.sittingStanding(sittingStanding.exam ?: "", sittingStanding)
            call.enqueue(object : Callback<SittingStanding> {
                override fun onResponse(call: Call<SittingStanding>, response: Response<SittingStanding>) {
                    sittingStanding.deleteFromRealm()
                }
                override fun onFailure(call: Call<SittingStanding>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun trunkFlexibilitySync(realm: Realm): Boolean {
        val query = realm.where(TrunkFlexibility::class.java)
        val results = query.findAll()
        for (trunkFlexibility in results.toList()) {
            trunkFlexibility.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.trunkFlexibility(trunkFlexibility.exam ?: "", trunkFlexibility)
            call.enqueue(object : Callback<TrunkFlexibility> {
                override fun onResponse(call: Call<TrunkFlexibility>, response: Response<TrunkFlexibility>) {
                    trunkFlexibility.deleteFromRealm()
                }
                override fun onFailure(call: Call<TrunkFlexibility>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun walkingSync(realm: Realm): Boolean {
        val query = realm.where(Walking::class.java)
        val results = query.findAll()
        for (walking in results.toList()) {
            walking.id = null
            val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
            val call = physicalExamService.walking(walking.exam ?: "", walking)
            call.enqueue(object : Callback<Walking> {
                override fun onResponse(call: Call<Walking>, response: Response<Walking>) {
                    walking.deleteFromRealm()
                }
                override fun onFailure(call: Call<Walking>, t: Throwable) {
                }
            })
        }
        return true
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    inner class SyncBinder : Binder() {
        internal // Return this instance of SyncBinder so clients can call public methods
        val service: SyncService
            get() = this@SyncService
    }

    companion object {
        val DELAY_TIME: Long = 60 * 1000
        var service: SyncService? = null
    }
}