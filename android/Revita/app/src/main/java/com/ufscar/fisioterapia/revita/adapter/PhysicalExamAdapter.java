package com.ufscar.fisioterapia.revita.adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ufscar.fisioterapia.revita.R;


public class PhysicalExamAdapter extends RecyclerView.Adapter<PhysicalExamAdapter.ViewHolder> {

    private Activity mActivity;
    private String[] mOptions;
    private OnCardInteractionListener mListener;

    public interface OnCardInteractionListener {
        void onInteraction(int position);
    }

    public PhysicalExamAdapter(Activity activity, OnCardInteractionListener listener) {
        this.mActivity = activity;
        this.mListener = listener;
        this.mOptions = activity.getResources().getStringArray(R.array.arr_physical_exam_options);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.physical_exam_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mContent.setText(mOptions[position]);
        holder.mCard.setOnClickListener(new View.OnClickListener() {
            int position;

            public View.OnClickListener setPosition(int position) {
                this.position = position;
                return this;
            }

            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onInteraction(position);
                }
            }
        }.setPosition(position));
    }

    @Override
    public int getItemCount() {
        return mOptions.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final CardView mCard;
        public final ImageView mStatus;
        public final AppCompatTextView mContent;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mCard = (CardView) view.findViewById(R.id.crd_option);
            mStatus = (ImageView) view.findViewById(R.id.img_status);
            mContent = (AppCompatTextView) view.findViewById(R.id.txt_content);
        }
    }
}



