package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class BodyMassHeight : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("bodyMass")
    var bodyMass: String? = null

    @SerializedName("height")
    var height: String? = null

    @SerializedName("bmi")
    var bmi: String? = null

    var exam: String? = null
}
