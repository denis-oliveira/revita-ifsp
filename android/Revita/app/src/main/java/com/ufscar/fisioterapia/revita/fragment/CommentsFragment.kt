package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.sub.BodyMassHeight
import com.ufscar.fisioterapia.revita.model.sub.Comments
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CommentsFragment : Fragment() {

    private var edtDropped: TextInputEditText? = null
    private var edtNumberFalls: TextInputEditText? = null
    private var edtMedicines: TextInputEditText? = null
    private var edtCurrentPhysicalActivity: TextInputEditText? = null
    private var edtOtherComments: TextInputEditText? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater!!.inflate(R.layout.fragment_comments, container, false)

        edtDropped = view?.findViewById(R.id.edt_dropped) as TextInputEditText?
        edtNumberFalls = view?.findViewById(R.id.edt_number_of_falls) as TextInputEditText?
        edtMedicines = view?.findViewById(R.id.edt_medicines) as TextInputEditText?
        edtCurrentPhysicalActivity = view?.findViewById(R.id.edt_current_physical_activity) as TextInputEditText?
        edtOtherComments = view?.findViewById(R.id.edt_other_comments) as TextInputEditText?

        val fragment = InformationFragment()
        fragment.information = context.getString(R.string.inst_comments)

        val btnHelp: AppCompatImageButton? = view?.findViewById(R.id.btn_help) as AppCompatImageButton
        btnHelp?.setOnClickListener {
            activity.supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_content_main, fragment)
                    .commit()
        }

        val btnAdd: AppCompatButton? = view?.findViewById(R.id.btn_add) as AppCompatButton
        btnAdd?.setOnClickListener {
            save()
        }

        return view
    }

    fun save() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val comments = Comments()
        comments.dropped = edtDropped?.text.toString()
        comments.numberFalls = edtNumberFalls?.text.toString()
        comments.medicines = edtMedicines?.text.toString()
        comments.currentPhysicalActivity = edtCurrentPhysicalActivity?.text.toString()
        comments.otherComments = edtOtherComments?.text.toString()

        val id = RevitaApplication.instance?.currentExam ?: ""

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.comments(id, comments)
        call.enqueue(object : Callback<Comments> {
            override fun onResponse(call: Call<Comments>, response: Response<Comments>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<Comments>, t: Throwable) {
                // Save to sync
                comments.exam = id
                val realm = DatabaseFactory.createInstance(context)
                realm.beginTransaction()
                realm.insertOrUpdate(comments)
                realm.commitTransaction()

                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Comments Fragment"))
    }

    companion object {
        private val TAG = "COMMENTS_FRAGMENT"
    }
}
