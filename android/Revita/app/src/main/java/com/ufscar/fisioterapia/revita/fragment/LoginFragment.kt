package com.ufscar.fisioterapia.revita.fragment

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.activity.MainActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.User
import com.ufscar.fisioterapia.revita.service.UserService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginFragment : Fragment(), View.OnClickListener {

    private var mEmailField: TextInputEditText? = null
    private var mPasswordField: TextInputEditText? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater?.inflate(R.layout.fragment_login, container, false)

        mEmailField = view?.findViewById(R.id.field_email) as TextInputEditText?
        mPasswordField = view?.findViewById(R.id.field_password) as TextInputEditText?

        view?.findViewById(R.id.email_sign_in_button)?.setOnClickListener(this)
        view?.findViewById(R.id.email_create_account_button)?.setOnClickListener(this)

        return view
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.email_create_account_button -> createAccount(
                    mEmailField?.text.toString(),
                    mPasswordField?.text.toString()
            )
            R.id.email_sign_in_button -> signIn(
                    mEmailField?.text.toString(),
                    mPasswordField!!.text.toString()
            )
        }
    }

    private fun createAccount(email: String, password: String) {
        Log.d(TAG, "createAccount:" + email)
        if (!validateForm()) {
            return
        }

        baseActivity.showProgressDialog()

        val user = User()
        user.username = email
        user.password = password

        val userService = NetworkFactory.createService(UserService::class.java)
        val call = userService.create(user)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                baseActivity.hideProgressDialog()
                val rUser = response.body()
                val realm = DatabaseFactory.createInstance(context)

                realm.beginTransaction()
                realm.insertOrUpdate(rUser)
                realm.commitTransaction()

                startMain()
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                baseActivity.hideProgressDialog()
            }
        })
    }

    private fun resetPasswordAccount(email: String) {
        Log.d(TAG, "createAccount:" + email)
        if (!validateForm()) {
            return
        }

        baseActivity.showProgressDialog()

        var user = User()
        user.username = email

        val userService = NetworkFactory.createService(UserService::class.java)
        val call = userService.reset(user)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                baseActivity.hideProgressDialog()
                startMain()
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                baseActivity.hideProgressDialog()
            }
        })
    }

    private fun signIn(email: String, password: String) {
        Log.d(TAG, "signIn:" + email)
        if (!validateForm()) {
            return
        }

        baseActivity.showProgressDialog()

        val user = User()
        user.username = email
        user.password = password

        val userService = NetworkFactory.createService(UserService::class.java)
        val call = userService.auth(user)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                baseActivity.hideProgressDialog()
                val rUser = response.body()

                if (rUser != null) {
                    val realm = DatabaseFactory.createInstance(context)
                    realm.beginTransaction()
                    realm.insertOrUpdate(rUser)
                    realm.commitTransaction()

                    startMain()

                    Crashlytics.setUserIdentifier(rUser.username)
                    Crashlytics.setUserName(rUser.name)
                    Crashlytics.setString(getString(R.string.crashlytics_profile), rUser.profile)

                    RevitaApplication.instance?.currentUser = rUser.username
                } else {
                    Toast.makeText(
                            context, R.string.err_authentication_fail, Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                baseActivity.hideProgressDialog()
                print(t)
                Log.d(TAG, "signIn:")
                Toast.makeText(
                        context, R.string.err_authentication_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun validateForm(): Boolean {
        var valid = true

        val email = mEmailField?.text.toString()
        if (TextUtils.isEmpty(email)) {
            mEmailField?.error = getString(R.string.required)
            valid = false
        } else {
            mEmailField?.error = null
        }

        val password = mPasswordField?.text.toString()
        if (TextUtils.isEmpty(password)) {
            mPasswordField?.error = getString(R.string.required)
            valid = false
        } else {
            mPasswordField?.error = null
        }

        return valid
    }

    private val baseActivity: BaseActivity
        get() = activity as BaseActivity

    private fun startMain() {
        MainActivity.start(activity)
        activity.finish()
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Login Fragment"))
    }

    companion object {
        private val TAG = "LOGIN_FRAGMENT"
    }
}
