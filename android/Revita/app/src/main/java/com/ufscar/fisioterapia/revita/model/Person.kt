package com.ufscar.fisioterapia.revita.model

class Person {

    var code: String? = null
    var name: String? = null
    var birthday: String? = null
    var genre: String? = null
    var phone: String? = null
    var status: String? = null

    constructor()

    constructor(code: String, name: String, birthday: String, genre: String, phone: String, status: String) {
        this.code = code
        this.name = name
        this.birthday = birthday
        this.genre = genre
        this.phone = phone
        this.status = status
    }
}
