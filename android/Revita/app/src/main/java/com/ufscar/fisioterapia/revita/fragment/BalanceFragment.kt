package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Chronometer
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.sub.Balance
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BalanceFragment : Fragment(), View.OnClickListener {

    private var running: Boolean = false

    private var chronometer: Chronometer? = null
    private var edtLegLeft: TextInputEditText? = null
    private var edtLegRight: TextInputEditText? = null
    private var edtTimeUpAndGo: TextInputEditText? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater!!.inflate(R.layout.fragment_balance, container, false)

        edtLegLeft = view?.findViewById(R.id.edt_leg_left) as TextInputEditText
        edtLegRight = view?.findViewById(R.id.edt_leg_right) as TextInputEditText
        edtTimeUpAndGo = view?.findViewById(R.id.edt_time_up_and_go) as TextInputEditText

        val fragment = InformationFragment()
        fragment.information = context.getString(R.string.inst_balance)

        val btnHelp: AppCompatImageButton? = view?.findViewById(R.id.btn_help) as AppCompatImageButton
        btnHelp?.setOnClickListener {
            activity.supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_content_main, fragment)
                    .commit()
        }

        chronometer = view?.findViewById(R.id.chronometer) as Chronometer?
        chronometer?.setOnClickListener(this)

        val btnAdd: AppCompatButton? = view?.findViewById(R.id.btn_add) as AppCompatButton
        btnAdd?.setOnClickListener(this)

        return view
    }

    override fun onClick(view: View?) {
        when(view?.id) {
            R.id.chronometer -> {
                if (running) {
                    chronometer?.stop()
                }
                else {
                    chronometer?.base = SystemClock.elapsedRealtime()
                    chronometer?.start()
                }
                running = !running
            }
            R.id.btn_add -> {
                save()
            }
        }
    }

    fun save() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val balance = Balance()
        balance.leftLeg = edtLegLeft?.text.toString()
        balance.rightLeg = edtLegRight?.text.toString()
        balance.timeUpAndGo = edtTimeUpAndGo?.text.toString()

        val id = RevitaApplication.instance?.currentExam ?: ""

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.balance(id, balance)
        call.enqueue(object : Callback<Balance> {
            override fun onResponse(call: Call<Balance>, response: Response<Balance>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<Balance>, t: Throwable) {
                // Save to sync
                balance.exam = id
                val realm = DatabaseFactory.createInstance(context)
                realm.beginTransaction()
                realm.insertOrUpdate(balance)
                realm.commitTransaction()

                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Balance Fragment"))
    }

    companion object {
        private val TAG = "BALANCE_FRAGMENT"
    }
}
