package com.ufscar.fisioterapia.revita.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R


class PhysicalAssessmentFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater!!.inflate(R.layout.fragment_physical_assessment,
                container, false)
        return view
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Physical Assessment Fragment"))
    }

    companion object {
        private val TAG = "PHYSICAL_ASSESSMENT_FRAGMENT"
    }
}
