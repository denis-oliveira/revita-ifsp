package com.ufscar.fisioterapia.revita.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ufscar.fisioterapia.revita.R

class ElderlyFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view: View? = inflater!!.inflate(R.layout.fragment_elderly, container, false)
        return view
    }

    companion object {
        private val TAG = "ELDERLY_FRAGMENT"
    }
}
