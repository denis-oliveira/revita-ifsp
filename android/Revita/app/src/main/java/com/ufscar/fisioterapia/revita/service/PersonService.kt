package com.ufscar.fisioterapia.revita.service

import com.ufscar.fisioterapia.revita.model.Person
import retrofit2.Call
import retrofit2.http.*

interface PersonService {
    @GET("api/person/{code}")
    fun get(@Path("code") code: String): Call<Person>

    @POST("api/person")
    fun create(@Body person: Person): Call<Person>

    @PUT("api/person")
    fun update(@Body person: Person): Call<Person>

    @DELETE("api/person")
    fun delete(@Body person: Person): Call<Boolean>
}