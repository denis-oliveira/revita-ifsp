package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class Balance : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("leftLeg")
    var leftLeg: String? = null

    @SerializedName("rightLeg")
    var rightLeg: String? = null

    @SerializedName("timeUpAndGo")
    var timeUpAndGo: String? = null

    var exam: String? = null
}
