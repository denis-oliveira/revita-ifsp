package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class BaseInformation : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("person")
    var person: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("time")
    var time: String? = null

    var exam: String? = null
}
