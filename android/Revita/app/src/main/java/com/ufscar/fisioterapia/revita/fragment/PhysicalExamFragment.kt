package com.ufscar.fisioterapia.revita.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.adapter.PhysicalExamAdapter


class PhysicalExamFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater!!.inflate(R.layout.fragment_physical_exam, container, false)

        val adapter = PhysicalExamAdapter(activity,
                PhysicalExamAdapter.OnCardInteractionListener { position ->
                    val ft = activity
                            .supportFragmentManager
                            .beginTransaction()
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    when (position) {
                        FRAGMENT_BLOOD_PRESSURE_AND_HEART_RATE -> {
                            ft.replace(R.id.fragment_content_main, BloodPressureAndHeartRateFragment())
                        }
                        FRAGMENT_BODY_MASS_HEIGHT -> {
                            ft.replace(R.id.fragment_content_main, BodyMassAndHeightFragment())
                        }
                        FRAGMENT_BIOIMPEDANCE -> {
                            ft.replace(R.id.fragment_content_main, BioimpedanceFragment())
                        }
                        FRAGMENT_SITTING_AND_STANDING -> {
                            ft.replace(R.id.fragment_content_main, SittingAndStandingFragment())
                        }
                        FRAGMENT_DYNAMOMETER_MANUAL -> {
                            ft.replace(R.id.fragment_content_main, DynamometerManualFragment())
                        }
                        FRAGMENT_TRUNK_FLEXIBILITY -> {
                            ft.replace(R.id.fragment_content_main, TrunkFlexibilityFragment())
                        }
                        FRAGMENT_BALANCE -> {
                            ft.replace(R.id.fragment_content_main, BalanceFragment())
                        }
                        FRAGMENT_WALKING -> {
                            ft.replace(R.id.fragment_content_main, WalkingFragment())
                        }
                        FRAGMENT_COMMENTS -> {
                            ft.replace(R.id.fragment_content_main, CommentsFragment())
                        }
                    }
                    ft.commit()
                })

        val recycler = view.findViewById(R.id.lst_options) as RecyclerView
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter

        return view
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Physical Exam Fragment"))
    }

    companion object {
        private val FRAGMENT_BLOOD_PRESSURE_AND_HEART_RATE = 0
        private val FRAGMENT_BODY_MASS_HEIGHT = 1
        private val FRAGMENT_BIOIMPEDANCE = 2
        private val FRAGMENT_SITTING_AND_STANDING = 3
        private val FRAGMENT_DYNAMOMETER_MANUAL = 4
        private val FRAGMENT_TRUNK_FLEXIBILITY = 5
        private val FRAGMENT_BALANCE = 6
        private val FRAGMENT_WALKING = 7
        private val FRAGMENT_COMMENTS = 8

        private val TAG = "PHYSICAL_EXAM_FRAGMENT"
    }
}
