package com.ufscar.fisioterapia.revita.model

import com.google.gson.annotations.SerializedName

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class PhysicalExam : RealmObject() {

    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    // Base Information
    @SerializedName("person")
    var person: String? = null
    @SerializedName("date")
    var date: String? = null
    @SerializedName("time")
    var time: String? = null

    // Blood Pressure and Heart Rate Fragment
    @SerializedName("bloodPressure")
    var bloodPressure: String? = null
    @SerializedName("systolic")
    var systolic: String? = null
    @SerializedName("diastolic")
    var diastolic: String? = null
    @SerializedName("heartRate")
    var heartRate: String? = null

    // Body Mass and Height Fragment
    @SerializedName("bodyMass")
    var bodyMass: String? = null
    @SerializedName("height")
    var height: String? = null
    @SerializedName("bmi")
    var bmi: String? = null

    // Bioimpedance Fragment
    @SerializedName("fatPercent")
    var fatPercent: String? = null
    @SerializedName("fatMass")
    var fatMass: String? = null

    // Sitting and Standing Fragment
    @SerializedName("sittingAndStanding")
    var sittingAndStanding: String? = null

    // Dynamometer Manual Fragment
    @SerializedName("handStrength")
    var handStrength: String? = null

    // Trunk Flexibility Fragment
    @SerializedName("trunkFlexibility")
    var trunkFlexibility: String? = null
    @SerializedName("abdominalCircumference")
    var abdominalCircumference: String? = null
    @SerializedName("hip")
    var hip: String? = null
    @SerializedName("waist")
    var waist: String? = null

    // Balance Fragment
    @SerializedName("leftLeg")
    var leftLeg: String? = null
    @SerializedName("rightLeg")
    var rightLeg: String? = null
    @SerializedName("timeUpAndGo")
    var timeUpAndGo: String? = null

    // Walking Fragment
    @SerializedName("numberLaps")
    var numberLaps: String? = null
    @SerializedName("metersTraveled")
    var metersTraveled: String? = null

    // Comments Fragment
    @SerializedName("dropped")
    var dropped: String? = null
    @SerializedName("numberFalls")
    var numberFalls: String? = null
    @SerializedName("medicines")
    var medicines: String? = null
    @SerializedName("currentPhysicalActivity")
    var currentPhysicalActivity: String? = null
    @SerializedName("otherComments")
    var otherComments: String? = null
}
