package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.os.Vibrator
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Chronometer
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.sub.Walking
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class WalkingFragment : Fragment(), Chronometer.OnChronometerTickListener {

    private var chronometer: Chronometer? = null
    private var edtNumberOfTurns: TextInputEditText? = null
    private var edtMeters: TextInputEditText? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater!!.inflate(R.layout.fragment_walking, container, false)

        edtNumberOfTurns = view?.findViewById(R.id.edt_number_of_turns) as TextInputEditText
        edtMeters = view?.findViewById(R.id.edt_meters) as TextInputEditText

        val fragment = InformationFragment()
        fragment.information = context.getString(R.string.inst_walking)

        val btnHelp: AppCompatImageButton? = view?.findViewById(R.id.btn_help) as AppCompatImageButton
        btnHelp?.setOnClickListener {
            activity.supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_content_main, fragment)
                    .commit()
        }

        chronometer = view?.findViewById(R.id.chronometer) as Chronometer?
        chronometer?.onChronometerTickListener = this
        chronometer?.setOnClickListener {
            chronometer?.base = SystemClock.elapsedRealtime()
            chronometer?.start()
            it.setOnClickListener(null)
        }

        val btnAdd: AppCompatButton? = view?.findViewById(R.id.btn_add) as AppCompatButton
        btnAdd?.setOnClickListener {
            save()
        }

        return view
    }

    override fun onChronometerTick(chronometer: Chronometer?) {
        if (chronometer?.text.toString() == "06:00") {
            chronometer?.stop()
            Toast.makeText(context, R.string.msg_finish, Toast.LENGTH_SHORT).show()
            val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(longArrayOf(0, 100, 100, 300, 100, 100), -1)
        } else if (chronometer?.text.toString() == "05:55") {
            Toast.makeText(context, R.string.msg_time_is_ending, Toast.LENGTH_SHORT).show()
            val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(300)
        } else if (chronometer?.text.toString().matches(Regex("\\d\\d:55"))) {
            Toast.makeText(context, R.string.msg_in_progress, Toast.LENGTH_SHORT).show()
            val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(300)
        }
    }

    fun save() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val walking = Walking()
        walking.numberLaps = edtNumberOfTurns?.text.toString()
        walking.metersTraveled = edtMeters?.text.toString()

        val id = RevitaApplication.instance?.currentExam ?: ""

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.walking(id, walking)
        call.enqueue(object : Callback<Walking> {
            override fun onResponse(call: Call<Walking>, response: Response<Walking>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<Walking>, t: Throwable) {
                // Save to sync
                walking.exam = id
                val realm = DatabaseFactory.createInstance(context)
                realm.beginTransaction()
                realm.insertOrUpdate(walking)
                realm.commitTransaction()

                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Walking Fragment"))
    }

    companion object {
        private val TAG = "WALKING_FRAGMENT"
    }
}
