package com.ufscar.fisioterapia.revita.application

import android.app.Application


class RevitaApplication : Application() {

    var currentExam: String? = null
    var currentUser: String? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        var instance: RevitaApplication? = null
            private set
    }
}
