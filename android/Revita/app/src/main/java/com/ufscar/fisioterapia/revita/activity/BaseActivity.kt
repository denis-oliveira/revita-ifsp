package com.ufscar.fisioterapia.revita.activity

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import com.ufscar.fisioterapia.revita.R

open class BaseActivity : AppCompatActivity() {

    private var mProgressDialog: ProgressDialog? = null

    fun showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog(this)
            mProgressDialog?.setMessage(getString(R.string.loading))
            mProgressDialog?.isIndeterminate = true
        }

        mProgressDialog?.show()
    }

    fun hideProgressDialog() {
        if (mProgressDialog!!.isShowing) {
            mProgressDialog?.hide()
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        mProgressDialog?.dismiss()
    }

}
