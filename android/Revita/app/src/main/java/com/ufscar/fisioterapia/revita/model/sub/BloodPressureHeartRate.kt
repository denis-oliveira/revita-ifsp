package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class BloodPressureHeartRate : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("bloodPressure")
    var bloodPressure: String? = null

    @SerializedName("systolic")
    var systolic: String? = null

    @SerializedName("diastolic")
    var diastolic: String? = null

    @SerializedName("heartRate")
    var heartRate: String? = null

    var exam: String? = null
}
