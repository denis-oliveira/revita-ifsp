package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatImageButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.sub.BloodPressureHeartRate
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BloodPressureAndHeartRateFragment : Fragment() {

    private var edtSystolicPressure: TextInputEditText? = null
    private var edtDiastolicPressure: TextInputEditText? = null
    private var edtHeatRate: TextInputEditText? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater?.inflate(R.layout.fragment_blood_pressure_and_heart_rate,
                container, false)

        edtSystolicPressure = view?.findViewById(R.id.edt_systolic_pressure) as TextInputEditText
        edtDiastolicPressure = view?.findViewById(R.id.edt_diastolic_pressure) as TextInputEditText
        edtHeatRate = view?.findViewById(R.id.edt_heart_rate) as TextInputEditText

        val fragment = InformationFragment()
        fragment.information = context.getString(R.string.inst_blood_pressure_and_heart_rate)

        val btnHelp: AppCompatImageButton? = view?.findViewById(R.id.btn_help) as AppCompatImageButton
        btnHelp?.setOnClickListener {
            activity.supportFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.fragment_content_main, fragment)
                    .commit()
        }

        val btnAdd: AppCompatButton? = view?.findViewById(R.id.btn_add) as AppCompatButton
        btnAdd?.setOnClickListener {
            save()
        }

        return view
    }

    fun save() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val bloodPressureHeartRate = BloodPressureHeartRate()
        bloodPressureHeartRate.systolic = edtSystolicPressure?.text.toString()
        bloodPressureHeartRate.diastolic = edtDiastolicPressure?.text.toString()
        bloodPressureHeartRate.heartRate = edtHeatRate?.text.toString()

        val id = RevitaApplication.instance?.currentExam ?: ""

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.bloodPressureHeartRate(id, bloodPressureHeartRate)
        call.enqueue(object : Callback<BloodPressureHeartRate> {
            override fun onResponse(call: Call<BloodPressureHeartRate>, response: Response<BloodPressureHeartRate>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<BloodPressureHeartRate>, t: Throwable) {
                // Save to sync
                bloodPressureHeartRate.exam = id
                val realm = DatabaseFactory.createInstance(context)
                realm.beginTransaction()
                realm.insertOrUpdate(bloodPressureHeartRate)
                realm.commitTransaction()

                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Blood Pressure and Heart Rate Fragment"))
    }

    companion object {
        private val TAG = "BLOOD_PRESSURE_AND_HEART_RATE_FRAGMENT"
    }
}
