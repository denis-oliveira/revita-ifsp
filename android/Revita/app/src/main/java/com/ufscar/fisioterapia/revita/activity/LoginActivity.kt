package com.ufscar.fisioterapia.revita.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.Toolbar
import android.view.View
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.fragment.LoginFragment
import com.ufscar.fisioterapia.revita.fragment.SplashFragment
import io.fabric.sdk.android.Fabric


class LoginActivity : BaseActivity(), Runnable {

    private var toolbar: Toolbar? = null


    override fun run() {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_content_login, LoginFragment())
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit()
        toolbar?.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        Fabric.with(this, Answers())
        setContentView(R.layout.activity_login)
        toolbar = findViewById(R.id.toolbar) as Toolbar?
        setSupportActionBar(toolbar)

        if (findViewById(R.id.fragment_content_login) != null) {
            if (savedInstanceState != null) {
                return
            }

            toolbar?.visibility = View.GONE
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_content_login, SplashFragment())
                    .commit()

            Handler().postDelayed(this, 3000)
        }
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            context.startActivity(starter)
        }
    }
}
