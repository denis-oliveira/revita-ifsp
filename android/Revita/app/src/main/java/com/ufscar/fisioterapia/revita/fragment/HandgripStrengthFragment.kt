package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatButton
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R


class HandgripStrengthFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view: View? = inflater!!.inflate(R.layout.fragment_handgrip_strength, container, false)
        return view
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Handgrip Fragment"))
    }

    companion object {
        private val TAG = "HANDGRIP_STRENGTH_FRAGMENT"
    }
}
