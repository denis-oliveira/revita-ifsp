package com.ufscar.fisioterapia.revita.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.ufscar.fisioterapia.revita.R

class SplashFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_splash, container, false)
    }

    companion object {
        private val TAG = "SPLASH_FRAGMENT"
    }
}
