package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.activity.SimpleScannerActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.Person
import com.ufscar.fisioterapia.revita.model.PhysicalExam
import com.ufscar.fisioterapia.revita.model.sub.BaseInformation
import com.ufscar.fisioterapia.revita.service.PersonService
import com.ufscar.fisioterapia.revita.service.PhysicalExamService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class OpenFragment : Fragment(), View.OnClickListener {

    private var spnCode: AppCompatSpinner? = null
    private var edtCode: TextInputEditText? = null
    private var btnScan: AppCompatButton? = null
    private var btnCancel: AppCompatButton? = null
    private var btnOpen: AppCompatButton? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater?.inflate(R.layout.fragment_open, container, false)

        spnCode = view?.findViewById(R.id.spn_code) as AppCompatSpinner?
        edtCode = view?.findViewById(R.id.edt_code) as TextInputEditText?

        btnScan = view?.findViewById(R.id.btn_scan) as AppCompatButton?
        btnScan?.setOnClickListener(this)

        btnCancel = view?.findViewById(R.id.btn_cancel) as AppCompatButton?
        btnCancel?.setOnClickListener(this)

        btnOpen = view?.findViewById(R.id.btn_open) as AppCompatButton?
        btnOpen?.setOnClickListener(this)

        return view
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_scan -> scan()
            R.id.btn_cancel -> fragmentManager.popBackStack()
            R.id.btn_open -> open()
        }
    }

    fun scan() {
        val intent = Intent(context, SimpleScannerActivity::class.java)
        activity.startActivity(intent)
    }

    fun open() {
        hideKeyboard()

        val code = spnCode?.selectedItem.toString() + edtCode?.text.toString()
        open(code)
    }

    fun open(code: String) {
        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val personService = NetworkFactory.createService(PersonService::class.java)
        val call = personService.get(code)
        call.enqueue(object : Callback<Person> {
            override fun onResponse(call: Call<Person>, response: Response<Person>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, "Registro encontrado com sucesso.", Toast.LENGTH_LONG
                ).show()

                val rPerson = response.body()
                create(rPerson)
            }
            override fun onFailure(call: Call<Person>, t: Throwable) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, "Registro não encontrado tente novamente.", Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun create(person: Person) {
        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val date = Date()

        val baseInformation = BaseInformation()
        baseInformation.person = person.code
        baseInformation.date = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(date)
        baseInformation.time = SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)

        val physicalExamService = NetworkFactory.createService(PhysicalExamService::class.java)
        val call = physicalExamService.create(baseInformation)
        call.enqueue(object : Callback<BaseInformation> {
            override fun onResponse(call: Call<BaseInformation>, response: Response<BaseInformation>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, "Exame iniciado com sucesso.", Toast.LENGTH_LONG
                ).show()

                val rBaseInformation = response.body()
                RevitaApplication.instance?.currentExam = rBaseInformation.id

                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .replace(R.id.fragment_content_main, PhysicalExamFragment())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
            }
            override fun onFailure(call: Call<BaseInformation>, t: Throwable) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, "Exame não conseguiu ser iniciado, tente novamente.",
                        Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Open Fragment"))
    }

    companion object {
        private val TAG = "OPEN_FRAGMENT"
    }
}
