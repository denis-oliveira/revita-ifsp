package com.ufscar.fisioterapia.revita.content

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.ufscar.fisioterapia.revita.app.SyncService


class RevitaReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        if (SyncService.service != null && SyncService.service?.isRunning ?: false) {
            SyncService.service?.syncData()
        }
    }
}
