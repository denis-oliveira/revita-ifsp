package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class Walking : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("numberLaps")
    var numberLaps: String? = null

    @SerializedName("metersTraveled")
    var metersTraveled: String? = null

    var exam: String? = null
}
