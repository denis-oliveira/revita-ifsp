package com.ufscar.fisioterapia.revita.fragment


import android.app.DatePickerDialog
import android.content.Context
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatSpinner
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.Person
import com.ufscar.fisioterapia.revita.service.PersonService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CreateFragment : Fragment(), View.OnClickListener, View.OnFocusChangeListener,
        DatePickerDialog.OnDateSetListener {

    private var edtCode: TextInputEditText? = null
    private var edtName: TextInputEditText? = null
    private var spnGenre: AppCompatSpinner? = null
    private var edtBirthday: TextInputEditText? = null
    private var edtStatus: TextInputEditText? = null
    private var btnCreate: AppCompatButton? = null
    private var btnCancel: AppCompatButton? = null
    private var datePicker: DatePickerDialog? = null
    private var dateSend: String? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater?.inflate(R.layout.fragment_create, container, false)

        edtCode = view?.findViewById(R.id.edt_code) as TextInputEditText?
        edtName = view?.findViewById(R.id.edt_name) as TextInputEditText?
        spnGenre = view?.findViewById(R.id.spn_genre) as AppCompatSpinner?

        edtBirthday = view?.findViewById(R.id.edt_birthday) as TextInputEditText?
        edtBirthday?.setOnClickListener(this)
        edtBirthday?.onFocusChangeListener = this

        edtStatus = view?.findViewById(R.id.edt_status) as TextInputEditText?

        btnCreate = view?.findViewById(R.id.btn_create) as AppCompatButton?
        btnCreate?.setOnClickListener(this)

        btnCancel = view?.findViewById(R.id.btn_cancel) as AppCompatButton?
        btnCancel?.setOnClickListener(this)

        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        datePicker = DatePickerDialog(context, this, year, month, day)

        return view
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.edt_birthday -> {
                hideKeyboard()
                datePicker?.show()
            }
            R.id.btn_create -> createPerson()
            R.id.btn_cancel -> {
                hideKeyboard()
                fragmentManager.popBackStack()
            }
        }
    }

    override fun onFocusChange(view: View?, focus: Boolean) {
        if (view?.id == R.id.edt_birthday && focus) {
            hideKeyboard()
            datePicker?.show()
        }
    }

    override fun onDateSet(datePicker: DatePicker?, year: Int, month: Int, day: Int) {
        val text = "$day/${month + 1}/$year"
        dateSend = "${month + 1}/$day/$year"
        edtBirthday?.setText(text)
    }

    private fun createPerson() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val person = Person()
        person.code = edtCode?.text.toString()
        person.name = edtName?.text.toString()
        person.genre = spnGenre?.selectedItemPosition.toString()
        person.birthday = dateSend
        person.status = edtName?.text.toString()

        val personService = NetworkFactory.createService(PersonService::class.java)
        val call = personService.create(person)
        call.enqueue(object : Callback<Person> {
            override fun onResponse(call: Call<Person>, response: Response<Person>) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_success, Toast.LENGTH_LONG
                ).show()
                fragmentManager.popBackStack()
            }

            override fun onFailure(call: Call<Person>, t: Throwable) {
                baseActivity?.hideProgressDialog()
                Toast.makeText(
                        context, R.string.msg_send_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Create Fragment"))
    }

    companion object {
        private val TAG = "CREATE_FRAGMENT"
    }
}
