package com.ufscar.fisioterapia.revita.fragment


import android.content.Context
import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatButton
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.ufscar.fisioterapia.revita.R
import com.ufscar.fisioterapia.revita.activity.BaseActivity
import com.ufscar.fisioterapia.revita.application.RevitaApplication
import com.ufscar.fisioterapia.revita.factory.DatabaseFactory
import com.ufscar.fisioterapia.revita.factory.NetworkFactory
import com.ufscar.fisioterapia.revita.model.User
import com.ufscar.fisioterapia.revita.service.UserService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProfileFragment : Fragment(), View.OnClickListener {

    var edtUsername: TextInputEditText? = null
    var edtPassword: TextInputEditText? = null
    var edtName: TextInputEditText? = null
    var edtProfile: TextInputEditText? = null
    var btnUpdate: AppCompatButton? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater?.inflate(R.layout.fragment_profile, container, false)

        edtUsername = view?.findViewById(R.id.edt_username) as TextInputEditText?
        edtPassword = view?.findViewById(R.id.edt_password) as TextInputEditText?
        edtName = view?.findViewById(R.id.edt_name) as TextInputEditText?
        edtProfile = view?.findViewById(R.id.edt_profile) as TextInputEditText?

        btnUpdate = view?.findViewById(R.id.btn_update) as AppCompatButton?
        btnUpdate?.setOnClickListener(this)

        showValues()
        return view
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_update -> updateValues()
        }
    }

    fun showValues() {
        val realm = DatabaseFactory.createInstance(context)
        val query = realm.where(User::class.java)
        val result = query.equalTo(
                "username", RevitaApplication.instance?.currentUser ?: ""
        ).findFirst()
        val user = result

        edtUsername?.setText(user.username)
        edtName?.setText(user.name)
        edtProfile?.setText(user.profile)
    }

    fun updateValues() {
        hideKeyboard()

        val baseActivity = activity as BaseActivity?
        baseActivity?.showProgressDialog()

        val user = User()
        user.username = edtUsername?.text.toString()
        user.name = edtName?.text.toString()
        user.password = edtPassword?.text.toString()
        user.profile = edtProfile?.text.toString()

        val userService = NetworkFactory.createService(UserService::class.java)
        val call = userService.update(user)
        call.enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) {
                baseActivity?.hideProgressDialog()
                val rUser = response.body()

                if (rUser != null) {
                    val realm = DatabaseFactory.createInstance(context)
                    realm.beginTransaction()
                    realm.insertOrUpdate(rUser)
                    realm.commitTransaction()
                    Toast.makeText(
                            context, R.string.user_update_success, Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                            context, R.string.user_update_fail, Toast.LENGTH_LONG
                    ).show()
                }
            }

            override fun onFailure(call: Call<User>, t: Throwable) {
                baseActivity?.hideProgressDialog()
                print(t)
                Log.d(TAG, "updateValues:")
                Toast.makeText(
                        context, R.string.user_update_fail, Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    fun hideKeyboard() {
        val view = activity.currentFocus
        if (view != null) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Profile Fragment"))
    }

    companion object {
        private val TAG = "PROFILE_FRAGMENT"
    }
}
