package com.ufscar.fisioterapia.revita.model.sub

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

import io.realm.annotations.PrimaryKey

open class Bioimpedance : RealmObject() {
    @PrimaryKey
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("fatPercent")
    var fatPercent: String? = null

    @SerializedName("fatMass")
    var fatMass: String? = null

    var exam: String? = null
}
