package com.ufscar.fisioterapia.revita.fragment


import android.os.Build
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent

import com.ufscar.fisioterapia.revita.R

import java.util.Locale


class InformationFragment : Fragment() {

    var information: String? = null

    private var toSpeech: TextToSpeech? = null
    private var wbInformation: WebView? = null


    @Suppress("deprecation")
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        logAnswers()
        val view = inflater!!.inflate(R.layout.fragment_information, container, false)
        wbInformation = view.findViewById(R.id.wb_information) as WebView?
        wbInformation?.loadData(information, "text/html; charset=utf-8", "UTF-8")

        toSpeech = TextToSpeech(context, TextToSpeech.OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                toSpeech!!.language = Locale.getDefault()
            }
        })

        view.findViewById(R.id.btn_speech).setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                toSpeech!!.speak(information ?: "", TextToSpeech.QUEUE_FLUSH, null)
            } else {
                toSpeech!!.speak(information ?: "", TextToSpeech.QUEUE_FLUSH, null,
                        toSpeech!!.defaultEngine)
            }
        }

        return view
    }

    fun logAnswers() {
        Answers.getInstance().logCustom(CustomEvent("Information Fragment"))
    }

    companion object {
        private val TAG = "INFORMATION_FRAGMENT"
    }
}
