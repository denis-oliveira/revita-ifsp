#!/bin/bash

backup() {
    mongodump --out ./dump/
}

restore() {
    mongorestore ./dump/
}
