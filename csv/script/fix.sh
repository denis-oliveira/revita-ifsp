#!/bin/bash

for FILE in $(ls *.csv)
do
	sed -E 's/"([0-9]+),([0-9]+)"/\1.\2/g' -i $FILE
    sed -E 's/^([0-9]+)\ ([A-Z]+),/\1\2,/' -i $FILE
done
