#!/bin/bash

rm -rf data
mkdir -p data

script/fix.sh
script/altura.awk altura.csv
script/bioimp.awk bioimp.csv
script/bioimpe.awk bioimpe.csv
script/ca.awk ca.csv
script/caminhada.awk caminhada.csv
script/cintura.awk cintura.csv
script/codigo.awk codigo.csv
script/fc.awk fc.csv
script/flex.awk flex.csv
script/forca.awk forca.csv
script/imc.awk imc.csv
script/pad.awk pad.csv
script/pas.awk pas.csv
script/pda.awk pda.csv
script/pea.awk pea.csv
script/peso.awk peso.csv
script/quadril.awk quadril.csv
script/quedas.awk quedas.csv
script/sentarlevantar.awk sentarlevantar.csv
script/tug.awk tug.csv

cat data/person | while read LINE
do
    curl -H "Content-Type: application/json" -X PUT -d "$LINE" http://localhost:8080/person/restore/
done

cat data/restore | while read LINE
do
    curl -H "Content-Type: application/json" -X PUT -d "$LINE" http://localhost:8080/restore/
done
