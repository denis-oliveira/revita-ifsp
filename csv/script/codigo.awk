#!/usr/bin/awk -f
BEGIN {
	FS = ","
}
{
    if (NR > 3) {
        print "{ \"code\":\""$1"\", \"name\":\""$2"\", \"status\":\""$3"\" }" >> "data/person"
    }
}
END {}
