#!/usr/bin/awk -f
BEGIN {
	FS = ","
}
{
	if (NR == 3) {
		total = NF
		for (i = 5; i < NF; i++) {
			dates[i] = $i
		}
	} else if (NR > 3) {
		print "{ \"code\": \""$1"\", \"birthday\":\""$3"\", \"genre\":\""$4"\" }" >> "data/person"
		for (i = 5; i < total; i++) {
			print "{ \"code\": \""$1"\", \"date\":\""dates[i]"\", \"fatPercent\":\""$i"\" }" >> "data/restore"
		}
	}
}
END {}
