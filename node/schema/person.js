'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var personSchema = new Schema({
	id: ObjectId,
	code: String,
	name: String,
	genre: String,
	phone: String,
	birthday: Date,
	status: String
});

var Person = mongoose.model('Person', personSchema);

module.exports = Person;
