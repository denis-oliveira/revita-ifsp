'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var physicalAssessmentSchema = new Schema({
	id: ObjectId,

    // Base Information
    person: { type: Schema.Types.ObjectId, ref: 'Person' },
    date: Date,
    time: String,

    // Blood Pressure and Heart Rate
    bloodPressure: String,
    systolic: String,
    diastolic: String,
    heartRate: String,

    // Body Mass and Height
    bodyMass: String,
    height: String,
    bmi: String,

    // Bioimpedance
    fatPercent: String,
    fatMass: String,

    // Sitting and Standing
    sittingAndStanding: String,

    // Dynamometer Manual
    handStrength: String,

    // Trunk Flexibility
    trunkFlexibility: String,
    abdominalCircumference: String,
    hip: String,
    waist: String,

    // Balance
    leftLeg: String,
    rightLeg: String,
    timeUpAndGo: String,

    // Walking
    numberLaps: String,
    metersTraveled: String,

    // Comments
    dropped: String,
    numberFalls: String,
    medicines: String,
    currentPhysicalActivity: String,
    otherComments: String
});

var PhysicalAssessment = mongoose.model('PhysicalAssessment', physicalAssessmentSchema);

module.exports = PhysicalAssessment;
