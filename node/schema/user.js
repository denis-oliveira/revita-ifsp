'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

var userSchema = new Schema({
	id: ObjectId,
	username: { type: String, index: { unique: true }},
	password: String,
    name: String,
	profile: String
});

var User = mongoose.model('User', userSchema);

module.exports = User;