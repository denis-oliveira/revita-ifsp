'use strict';

var SimpleController = require('./simple-ctrl.js');
var Person = require('../schema/person.js');
var PhysicalAssessment = require('../schema/physical_assessment.js');
var Moment = require('moment');

class PhysicalAssessmentController extends SimpleController {
    constructor(app) {
        super(app, 'physical_assessment', PhysicalAssessment);
        this.query = PhysicalAssessment.find();
        this.configure();

        // Restore
        app.put('/restore/', (req, res) => {
            console.log('PUT::PhysicalAssessmentController::Restore => '); // + path);

            if (req.body.date) {
                req.body.date = Moment(req.body.date, "DD/MM/YYYY");
            }

            var person = Person
                .findOne()
                .where('code').equals(req.body.code)
                .select('_id')
                .exec()
                .then(function (person) {
                    req.body.person = person;
                    PhysicalAssessment
                        .findOne()
                        .where('person').equals(person)
                        .where('date').equals(req.body.date)
                        .exec()
                        .then(function (entity) {
                            entity = Object.assign(entity, req.body);
                            entity
                                .save()
                                .then(function (entity) {
                                    console.log(entity);
                                    res.send(entity);
                                }).catch(function (err) {
                                    console.error(err);
                                    res.send(err);
                                });
                        }).catch(function (err) {
                            var physicalAssessment = new PhysicalAssessment(req.body);
                            physicalAssessment
                                .save()
                                .then(function (entity) {
                                    console.log(enty);
                                    res.send(entity);
                                }).catch(function (err) {
                                    console.error(err);
                                    res.send(err);
                                });
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.put('/api/novo1', (req, res) => {
            console.log('PUT::PhysicalAssessmentController::Restore => '); // + path);

            if (req.body.date) {
                req.body.date = Moment(req.body.date, "DD/MM/YYYY");
            }

            var person = Person
                .findOne()
                .where('code').equals(req.body.code)
                .select('_id')
                .exec()
                .then(function (person) {
                    req.body.person = person;
                    PhysicalAssessment
                        .findOne()
                        .where('person').equals(person)
                        .where('date').equals(req.body.date)
                        .exec()
                        .then(function (entity) {
                            entity = Object.assign(entity, req.body);
                            entity
                                .save()
                                .then(function (entity) {
                                    console.log(entity);
                                    res.send(entity);
                                }).catch(function (err) {
                                    console.error(err);
                                    res.send(err);
                                });
                        }).catch(function (err) {
                            var physicalAssessment = new PhysicalAssessment(req.body);
                            physicalAssessment
                                .save()
                                .then(function (entity) {
                                    console.log(entity);
                                    res.send(entity);
                                }).catch(function (err) {
                                    console.error(err);
                                    res.send(err);
                                });
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Base Information
        app.get('/api/physical_assessment/:id/base', (req, res) => {
            console.log('GET::PhysicalAssessmentController::Base => '); // + path);

            var promise = PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('person date time')
                .populate('person')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err)
                });
        });

        app.post('/api/physical_assessment/base', (req, res) => {
            console.log('POST::PhysicalAssessmentController::Create => ');

            if (req.body.date) {
                req.body.date = Moment(req.body.date, "DD/MM/YYYY");
            }

            var person = Person
                .findOne()
                .where('code').equals(req.body.person)
                .select('_id')
                .exec()
                .then(function (person) {
                    req.body.person = person;
                    PhysicalAssessment
                        .findOne()
                        .where('person').equals(person)
                        .where('date').equals(req.body.date)
                        .exec()
                        .then(function (entity) {
                            if (entity) {
                                console.log(entity);
                                res.send(entity);
                            }
                            else {
                                var physicalAssessment = new PhysicalAssessment(req.body);
                                physicalAssessment
                                    .save()
                                    .then(function (entity) {
                                        console.log(entity);
                                        res.send(entity);
                                    }).catch(function (err) {
                                        console.error(err);
                                        res.send(err);
                                    });
                            }
                        }).catch(function (err) {
                            var physicalAssessment = new PhysicalAssessment(req.body);
                            physicalAssessment
                                .save()
                                .then(function (entity) {
                                    console.log(entity);
                                    res.send(entity);
                                }).catch(function (err) {
                                    console.error(err);
                                    res.send(err);
                                });
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });


        // Blood Pressure and Heart Rate
        app.get('/api/physical_assessment/:id/blood-pressure-heart-rate', (req, res) => {
            console.log('GET::PhysicalAssessmentController::BloodPressureHeartRate => '); // + path);

            PhysicalAssessment
                .findOne()
                .select('bloodPressure heartRate')
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err)
                });
        });

        app.post('/api/physical_assessment/:id/blood-pressure-heart-rate', (req, res) => {
            console.log('POST::PhysicalAssessmentController::BloodPressureHeartRate => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err)
                });
        });


        // Body Mass and Height
        app.get('/api/physical_assessment/:id/body-mass-height', (req, res) => {
            console.log('GET::PhysicalAssessmentController::BodyMassHeight => '); // + path);

            PhysicalAssessment
                .findOne()
                .select('bodyMass height')
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err)
                });
        });

        app.post('/api/physical_assessment/:id/body-mass-height', (req, res) => {
            console.log('POST::PhysicalAssessmentController::BodyMassHeight => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Bioimpedance
        app.get('/api/physical_assessment/:id/bioimpedance', (req, res) => {
            console.log('GET::PhysicalAssessmentController::Bioimpedance => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('fatPercent fatMass')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err)
                });
        });

        app.post('/api/physical_assessment/:id/bioimpedance', (req, res) => {
            console.log('POST::PhysicalAssessmentController::Bioimpedance => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Sitting and Standing
        app.get('/api/physical_assessment/:id/sitting-standing', (req, res) => {
            console.log('GET::PhysicalAssessmentController::SittingStanding => '); // + path);

            PhysicalAssessment
                .findOne()
                .select('sittingAndStanding')
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/sitting-standing', (req, res) => {
            console.log('POST::PhysicalAssessmentController::SittingStanding => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Dynamometer Manual
        app.get('/api/physical_assessment/:id/dynamometer-manual', (req, res) => {
            console.log('GET::PhysicalAssessmentController::DynamometerManual => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('leftHand rightHand dexterous sinister')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/dynamometer-manual', (req, res) => {
            console.log('POST::PhysicalAssessmentController::DynamometerManual => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Trunk Flexibility
        app.get('/api/physical_assessment/:id/trunk-flexibility', (req, res) => {
            console.log('GET::PhysicalAssessmentController::TrunkFlexibility => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('trunkFlexibility abdominalCircumference hip')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/trunk-flexibility', (req, res) => {
            console.log('POST::PhysicalAssessmentController::TrunkFlexibility => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Balance
        app.get('/api/physical_assessment/:id/balance', (req, res) => {
            console.log('GET::PhysicalAssessmentController::Balance => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('leftLeg rightLeg timeUpAndGo')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/balance', (req, res) => {
            console.log('POST::PhysicalAssessmentController::Balance => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Walking
        app.get('/api/physical_assessment/:id/walking', (req, res) => {
            console.log('GET::PhysicalAssessmentController::Walking => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('numberLaps metersTraveled')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/walking', (req, res) => {
            console.log('POST::PhysicalAssessmentController::Walking => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Comments
        app.get('/api/physical_assessment/:id/comments', (req, res) => {
            console.log('GET::PhysicalAssessmentController::Comments => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .select('dropped numberFalls medicines currentPhysicalActivity otherComments')
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        app.post('/api/physical_assessment/:id/comments', (req, res) => {
            console.log('POST::PhysicalAssessmentController::Comments => '); // + path);

            PhysicalAssessment
                .findOne()
                .where('_id').equals(req.params.id)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });
    }
}

module.exports = PhysicalAssessmentController;
