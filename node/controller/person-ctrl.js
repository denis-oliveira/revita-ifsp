'use strict';

var SimpleController = require('./simple-ctrl.js');
var Person = require('../schema/person.js');
var Moment = require('moment');

class PersonController extends SimpleController {
    constructor(app) {
        super(app, 'person', Person);
        this.query = Person.find();
        this.configure();

        // GET PERSON
        app.get('/api/person/:code', (req, res) => {
            var code = req.params.code;
            var promise = Person
                .findOne()
                .where("code").equals(code)
                .exec()
                .then(function (entity) {
                    console.log(entity);
                    res.send(entity);                    
                }).catch(function (err) {
                    console.error(err);
                    res.send(err);
                });
        });

        // Restore
        app.put('/person/restore/', (req, res) => {
            console.log('PUT::PersonController::Restore => '); // + path);

            var query = {
                code: req.body.code,
            };

            if (req.body.birthday) {
                req.body.birthday = Moment(req.body.birthday, "DD/MM/YYYY");
            }

            var promise = Person
                .findOne()
                .where("code").equals(req.body.code)
                .exec()
                .then(function (entity) {
                    entity = Object.assign(entity, req.body);
                    entity
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                }).catch(function (err) {
                    var person = new Person(req.body);
                    person
                        .save()
                        .then(function (entity) {
                            console.log(entity);
                            res.send(entity);
                        }).catch(function (err) {
                            console.error(err);
                            res.send(err);
                        });
                });
        });
    }
}

module.exports = PersonController;
