'use strict';

const base = '/api/';

class SimpleController {
    constructor(app, endpoint, entity) {
        var path = base + endpoint;
        var Entity = entity;

        // GET method
        this.get = (req, res) => {
            console.log('GET::SimpleController => ' + path);
            
            this.query.exec(function (err, entities) {
                if (err) return console.error(err);
                res.send(entities);
            });
        };

        // POST method
        this.post = (req, res) => {
            console.log('POST::SimpleController => ' + path);
            console.log(req.body);

            var entity = new Entity(req.body);
            entity.save(function (err, entity) {
                if (err) {
                    res.send("ERROR");
                    return console.error(err);
                }
                else {
                    console.dir(entity);
                    res.send(entity);
                }
            });
        };

        // PUT method
        this.put = (req, res) => {
            console.log('PUT::SimpleController => ' + path);
            console.log(req.body);

            if (!req.body._id) {
                console.log("ERROR");
                this.error(res, "WITHOUT_ID");
                return;
            }

            var query = {
                _id: req.body._id
            };

            Entity.update(query, req.body, function (err) {
                if (err) {
                    res.send("ERROR");
                    return console.error(err);
                }
                else {
                    Entity.findOne(query, function (err, entity) {
                        console.dir(entity);
                        res.send(entity);
                    });
                }
            });
        };

        // DELETE method
        this.delete = (req, res) => {
            console.log('DELETE::SimpleController => ' + path);
            console.log(req.body);

            if (!req.body._id) {
                console.log("ERROR");
                this.error(res, "WITHOUT_ID");
                return;
            }

            var query = {
                _id: req.body._id
            }

            Entity.remove(query, function (err) {
                if (err) {
                    res.status(403);
                    res.send(err);
                }
                else {
                    res.send(query);
                }
            });
        };

        // PATCH method
        this.patch = (req, res) => {
            console.log('PATCH::SimpleController => ' + path);
            this.query.exec(function (err, entities) {
                if (err) return console.error(err);
                res.send(entities);
            });
        };

        this.configure = () => {
            console.log('Register: GET::' + path);
            app.get(path, this.get);

            console.log('Register: POST::' + path);
            app.post(path, this.post);

            console.log('Register: PUT::' + path);
            app.put(path, this.put);

            console.log('Register: DELETE::' + path);
            app.delete(path, this.delete);

            console.log('Register: PATCH::' + path);
            app.patch(path, this.patch);
        }

        this.error = (res, msg) => {
            res.status(403);
            res.send(msg);
        }
    }
}

module.exports = SimpleController;
