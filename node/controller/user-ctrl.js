'use strict';

var SimpleController = require('./simple-ctrl.js');
var User = require('../schema/user.js');
var MD5 = require('crypto-js/md5');

class UserController {
    constructor(app) {
        this.error = (res, msg) => {
            res.status(403);
            res.send(msg);
        }

        app.post('/user/auth', (req, res) => {
            if (!req.body.username) {
                this.error(res, '{"error":"Invalid"}');
                return;
            }

            if (!req.body.password) {
                this.error(res, '{"error":"Invalid"}');
                return
            }

            var promise = User
                .findOne()
                .where('username').equals(req.body.username)
                .where('password').equals(MD5(req.body.password))
                .select('_id username name profile')
                .exec()
                .then(function (entity) {
                    if (entity) {
                        res.send(entity);
                        console.log(entity);
                    } else {
                        res.status(403);
                        res.send('{"error":"Invalid"}');
                    }
                }).catch(function (err) {
                    res.status(403);
                    res.send(err)
                    console.error(err);
                });
        });

        app.post('/user/', (req, res) => {
            if (!req.body.username) {
                this.error(res, '{"error":"Invalid"}');
                return;
            }

            if (!req.body.password) {
                this.error(res, '{"error":"Invalid"}');
                return;
            }
            req.body.password = MD5(req.body.password);

            var promise = User(req.body)
                .save()
                .then(function (entity) {
                    if (entity) {
                        res.send(entity);
                        console.log(entity);
                    } else {
                        res.status(403);
                        res.send('{"error":"Invalid"}');
                    }
                }).catch(function (err) {
                    res.send(err)
                    console.error(err);
                });
        });

        app.put('/user/', (req, res) => {
            if (!req.body.username) {
                this.error(res, '{"error":"Invalid"}');
                return;
            }

            if (!req.body.password) {
                this.error(res, '{"error":"Invalid"}');
                return;
            }

            var promise = User
                .findOne()
                .where('username').equals(req.body.username)
                .select('_id username name profile')
                .exec()
                .then(function (entity) {
                    if (req.body.password) entity.password = MD5(req.body.password);
                    if (req.body.name) entity.name = req.body.name;
                    if (req.body.profile) entity.profile = req.body.profile;

                    entity.save().then(function (entity) {
                        if (entity) {
                            res.send(entity);
                            console.log(entity);
                        } else {
                            res.status(403);
                            res.send('{"error":"Invalid"}');
                        }
                    });
                }).catch(function (err) {
                    res.send(err)
                    console.error(err);
                });
        });
    }
}

module.exports = UserController;
