// Controller Imports
var UserController = require('./controller/user-ctrl.js');
var PersonController = require('./controller/person-ctrl.js');
var PhysicalAssessmentController = require('./controller/physical_assessment-ctrl.js');

const controllers = (app) => {
	new UserController(app);
	new PersonController(app);
	new PhysicalAssessmentController(app);
}

module.exports = controllers;
