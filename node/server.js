'use strict';

// Configuration
const host = require('./config.js');
const controllers = require('./controllers.js');

// Library Imports
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json()); // parsing application/json 

// Configure CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH");
    next();
});

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var db = mongoose.connect(host).connection;

// Configure Mongodb
db.on('error', console.error);
db.once('open', function () {
    console.log('open connection');
});

// Configure Controllers
controllers(app);

// Server Start
var server = app.listen(8080, () => {
    var host = server.address().address;
    var port = server.address().port;

    console.log("Server start at http://%s:%s", host, port);
});
